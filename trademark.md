You may do the following without receiving specific permission from Trovu (or its affiliates):

* Use the Trovu name and logo on your own website if it refers directly to our products and services. This includes use on software portals, etc.
  *Use the Trovu name and logo to promote the browser on your own web properties, as long as you make it clear you are not affiliated with Trovu in any way.

All other uses of a Trovu trademark require our prior written permission. This includes, without limiting the generality of the above, any use of a Trovu trademark in a domain name. Contact us at trovu@protonmail.com for more information.

