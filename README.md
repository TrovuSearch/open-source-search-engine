# Trovu - an open source search engine

An open source web and enterprise search engine and spider/crawler.

Trovu is a fork of the [Findx](https://github.com/privacore/open-source-search-engine) search engine project

## SUPPORTED PLATFORMS
### Primary:
*    Ubuntu 16.04, g++ 5.4.0, Python 2.7.6

### Secondary:
*    OpenSuSE 13.2, GCC 4.8.3
*    OpenSuSE 42.2, GCC 6.2.1
*    Fedora 25, GCC 6.3.1

## DEPENDENCIES
### Compilation
#### Ubuntu
*    g++
*    make
*    cmake
*    python
*    libpcre3-dev
*    libssl-dev
*    libprotobuf-dev
*    protobuf-compiler
*    libsqlite3-dev

#### OpenSuse
*    g++
*    make
*    cmake
*    python
*    pcre-devel
*    libssl-dev
*    protobuf-devel
*    libprotobuf13

#### Fedora
*    g++
*    make
*    cmake
*    python
*    pcre-devel
*    openssl-devel
*    protobuf-devel
*    protobuf-compiler
*    sqlite-devel

### Runtime
*    Multi-instance installations require [Vagus](https://github.com/privacore/vagus) for keeping track of which instances are dead and alive.

#### Ubuntu
*    libssl1.0.0
*    libpcre3
*    libprotobuf9v5

## Installation
##Build
git clone https://gitlab.com/TrovuSearch/open-source-search-engine

git submodule init 

git submodule update

make -j4

make dist

./gb install

./gb start

## RUNNING THE SEARCH ENGINE
See <a href=html/faq.html>html/faq.html</a> for all administrative documentation including the quick start instructions.

## CODE ARCHITECTURE
See <a href=html/developer.html>html/developer.html</a> for all code documentation.

## SUPPORT
Trovu does not provide paid support. We provide limited support, primarily for active contributors.

## TRADEMARK POLICY

Trovu and the Trovu logo are trademarks of Tomáš Dienstbier in Czech republic and other countries.

Although our code is free, it is very important to note that we strictly enforce our trademark (™) rights, in order to be able to protect our users against people who use the marks to commit fraud. Our trademarks include, among others, the name Trovu™, as well as the Trovu™ logo and project names. This means that, while you have considerable freedom to redistribute and modify our software, there are tight restrictions on your ability to use the Trovu names and logos in ways which fall in the domain of trademark law, even when included in the source tree and built into binaries that we provide for distribution.
